\select@language {ngerman}
\select@language {ngerman}
\contentsline {chapter}{Zeichen, Benennungen und Einheiten}{2}{chapter*.2}
\contentsline {chapter}{Vorwort}{3}{chapter*.6}
\contentsline {chapter}{\numberline {1}Einleitung}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Stand der Technik}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}ML und Python}{5}{section.2.1}
\contentsline {chapter}{\numberline {3}Pr\IeC {\"a}zisierung der Aufgbenstellung}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Universality Theorem}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Die F\IeC {\"a}higkeit beliebige Funktionen zu berechnen}{8}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Universilaty mit einem Eingang und einem Ausgang anschaulich}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Universalit\IeC {\"a}t formal}{10}{subsection.4.1.2}
\contentsline {chapter}{\numberline {5}Die Ergebnisse eines Deep Neural Net's (DNN) verbessern}{12}{chapter.5}
\contentsline {chapter}{\numberline {6}2. Hauptabschnitt}{13}{chapter.6}
\contentsline {section}{\numberline {6.1}Zusammenfassung}{13}{section.6.1}
\contentsline {chapter}{\numberline {7}Gesamtzusammenfassung und weiterf\IeC {\"u}hrende Aufgaben}{14}{chapter.7}
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{15}{chapter*.13}
\contentsline {chapter}{Anlagen}{16}{chapter*.14}

import numpy as np

def sigmoid(z):
	return 1./(1+np.exp(-z))
NoExamples = 800
z = np.zeros((NoExamples,1))
for i,temp in enumerate(np.linspace(-5,5,NoExamples)):
	z[i,0] = temp
print('Normal')
print('a,b')
for i,temp in enumerate(sigmoid(z)):
	print('%f, %f'%(z[i][0],temp[0]))

z = np.zeros((NoExamples,1))
for i,temp in enumerate(np.linspace(-5,5,NoExamples)):
	z[i,0] = temp
print('Step like')
print('a,b')
for i,temp in enumerate(np.linspace(-5,5,NoExamples)):
	print('%f, %f'%(temp,sigmoid(temp*100 - 250)))
print('a,b')
for i,temp in enumerate(np.linspace(-5,5,NoExamples/10)):
	print('%f, %f'%(temp,sigmoid(temp*10 - 25)))
print('a,b')
for i,temp in enumerate(np.linspace(-5,5,NoExamples/50)):
	print('%f, %f'%(temp,sigmoid(temp*1 - 2.5)))

print('Zielfkt.')
for i,temp in enumerate(np.linspace(-5,5,NoExamples/20)):
	print('%f, %f'%(temp,np.sin(i)*temp+ i*temp -np.cos(i)*i))
print('Approximation')
print('a, b')
for i,temp in enumerate(np.linspace(-5,5,NoExamples/20)):
	print('%f, %f'%(temp-100*NoExamples**-1,0))
	print('%f, %f'%(temp-100*NoExamples**-1,np.sin(i)*temp+ i*temp -np.cos(i)*i))
	print('%f, %f'%(temp+100*NoExamples**-1,np.sin(i)*temp+ i*temp -np.cos(i)*i))
	print('%f, %f'%(temp+100*NoExamples**-1,0))

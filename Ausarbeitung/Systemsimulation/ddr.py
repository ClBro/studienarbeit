import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy.integrate import odeint
import matplotlib.animation as animation
import numpy.random as random
from collections import defaultdict
from time import time
class ddr:
	def __init__(self, v, d, lcab, dclutch, ltrailer):
		self.v = v		
		self.d = d
		self.dclutch = dclutch
		self.lcab = lcab
		self.ltrailer = ltrailer
		self.thetawheel = random.random()
	def ddrInit(self,xdrive, ydrive, thetacab, thetatrailer, thetawheel):
		rcab = [[xdrive-self.lcab*np.cos(thetacab)],[ydrive - self.lcab*np.sin(thetacab)]]
		rclutch = [[rcab[0] - self.dclutch*np.cos(thetacab)],[rcab[1] - self.dclutch*np.sin(thetacab)]]
		rtrailer = [[rclutch[0] - self.ltrailer*np.cos(thetatrailer)],[rclutch[1] - self.ltrailer*np.sin(thetatrailer)]]
		thetadrive=thetacab
		
		return [x for x in [xdrive, ydrive, thetadrive, rcab[0][0], rcab[1][0], thetacab,rclutch[0][0][0], rclutch[1][0][0], rtrailer[0][0][0][0], rtrailer[1][0][0][0], thetawheel]]
	def ddr(self,x, t):
		x1, x2, theta = x[0], x[1], x[2]
		x1cab, x2cab, thetacab = x[3], x[4], x[5]
		x1clutch, x2clutch = x[6], x[7]
		x1trailer, x2trailer = x[8], x[9]
		sthetacab, cthetacab = np.sin(thetacab), np.cos(thetacab)
		# Kinematik der Räder auf Antriebsachse
		sthetawheel, cthetawheel = np.sin(self.thetawheel), np.cos(self.thetawheel)
		xdotdrive = np.matrix([[self.v*(cthetacab*cthetawheel-sthetacab*sthetawheel)],
                               [self.v*(cthetacab*sthetawheel+sthetacab*cthetawheel)],
                               [self.lcab**-1*self.v*sthetawheel]])
		# Kinematik der Kabine
		xdotcab = np.matrix([[cthetacab**2, sthetacab*cthetacab],
		                     [sthetacab*cthetacab, sthetacab**2]]).dot(xdotdrive[0:2])[:]
		x1dot, x2dot, = xdotdrive[0], xdotdrive[1]
		x1cabdot, x2cabdot = xdotcab[0], xdotcab[1]
		thetadotcab = ((x2dot-x2cabdot)*(x1-x1cab)-(x1dot-x1cabdot)*(x2-x2cab))*((x1-x1cab)**2 + (x2-x2cab)**2)**-1
		# Kinematik der Kupplung
		xdotclutch = xdotcab[0:2] + np.matrix([[sthetacab],[-cthetacab]])*thetadotcab*self.dclutch
		# Kinematik Anhänger
		thetatrailer = np.arctan2(x2clutch-x2trailer, x1clutch-x1trailer)
		cthetatrailer, sthetatrailer = np.cos(thetatrailer), np.sin(thetatrailer)
		xdottrailer = np.matrix([[cthetatrailer**2, sthetatrailer*cthetatrailer],
		                         [sthetatrailer*cthetatrailer, sthetatrailer**2]]).dot(xdotclutch[0:2])[:]
		return [float(xdotdrive[0]), float(xdotdrive[1]), float(xdotdrive[2]), 
		        float(xdotcab[0]), float(xdotcab[1]), thetadotcab,
		        float(xdotclutch[0]), float(xdotclutch[1]),
		        float(xdottrailer[0]), float(xdottrailer[1]), self.thetawheel]


fig = plt.figure()
ax = plt.axes(xlim=(-25, 25), ylim=(-25, 25))
plt.gca().set_aspect('equal', adjustable='box')

class Animation:
	def __init__(self, ax):
		self.drive, = ax.plot([], [], lw=2)
		self.cab, = ax.plot([], [], lw=2)
		self.clutch, = ax.plot([], [], lw=2)
		self.trailer, = ax.plot([], [], lw=2)
	def AniInit(self):
		self.drive.set_data([], [])
		self.cab.set_data([], [])
		self.clutch.set_data([], [])
		self.trailer.set_data([], [])
		return self.drive, self.cab, self.clutch, self.trailer

	# animation function.  This is called sequentially
	def animate(self,i,sol): 
		rxdrive, rydrive, thetadrive = sol[i,0], sol[i,1], sol[i,2]
		rxcab, rycab, thetacab = sol[i,3], sol[i,4], sol[i,5]
		rxclutch, ryclutch = sol[i,6], sol[i,7]
		rxtrailer, rytrailer = sol[i,8], sol[i,9]
		x = [rxdrive-np.sin(thetadrive), rxdrive+np.sin(thetadrive)]
		y = [ rydrive+np.cos(thetadrive), rydrive-np.cos(thetadrive)]
		#thetacab = np.arctan2(rydrive - rycab, rxdrive - rxcab)
		xcab = [rxcab-np.sin(thetacab),rxcab,rxdrive,rxcab, rxcab+np.sin(thetacab)]
		ycab = [rycab+np.cos(thetacab),rycab,rydrive,rycab, rycab-np.cos(thetacab)]
		xclutch = [rxcab, rxclutch]
		yclutch = [rycab, ryclutch]
		thetatrailer = np.arctan2(ryclutch-rytrailer,rxclutch-rxtrailer)
		xtrailer = [rxtrailer - np.sin(thetatrailer), rxtrailer,rxclutch,rxtrailer, rxtrailer+np.sin(thetatrailer)]
		ytrailer = [rytrailer+np.cos(thetatrailer),rytrailer,ryclutch,rytrailer,rytrailer-np.cos(thetatrailer)]
		self.drive.set_data(x, y)
		self.cab.set_data(xcab,ycab)
		self.clutch.set_data(xclutch,yclutch)
		self.trailer.set_data(xtrailer,ytrailer)
		return self.drive, self.cab, self.clutch, self.trailer

# call the animator.  blit=True means only re-draw the parts that have changed.
# SImulationsparameter
t0 = 0
NoSteps = 100
t = np.linspace(0,100,NoSteps)

# Modell
systems = defaultdict()
systems['ddr'] = defaultdict()
systems['gen'] =  defaultdict()
for i in range(0,500000):
	systems['ddr'][i] = ddr(-.5,1,1,.1,5)
systems['gen']['n'] = len(systems['ddr'])
systems['x0']  = defaultdict()
for i in range(0,systems['gen']['n']):
	systems['x0'][i] = systems['ddr'][i].ddrInit(10*random.randn(1,1)[0][0],10*random.randn(1,1)[0][0],random.randn(1,1)[0][0],random.randn(1,1)[0][0], random.random()) 

n = systems['gen']['n']
# Speichern der imulationsergebnisse

solutions = defaultdict()
solutions['ddr'] = defaultdict()
tic = time()
for i in range(0,n):
	tic1 = time()
	solutions['ddr'][i] = odeint(systems['ddr'][i].ddr,systems['x0'][i], t)
	tic2 = time()
	print('System %d wurde in %f s berechnet.'%(i+1,tic2-tic1))
print('Integration nach %f s beendet.'%(tic2-tic))

#Aufbereiten der Daten für NN
# Speichert Daten in np.array mit namen dataset und output auf disk
savedatabool = True
if savedatabool == True:
	tic1 = time()
	dataset = np.zeros((int(n*0.5*NoSteps),7))
	output = np.zeros((int(n*0.5*NoSteps),6))
	k = 0
	decind = 0

	for i in solutions['ddr']:
		for temp in solutions['ddr'][i]:
			if k%2 == 0:
				dataset[k - decind,0:7] = np.delete(temp, (0,1,3,4), axis = 0)
				decind = decind + 1
			else:
				output [k - decind,:] = np.delete(temp, (0,1,3,4,10), axis = 0)
			k = k + 1
	#print(dataset)
	try:
		np.savez('dataset', dataset)
		np.savez('output', output)
		tic2 = time()
		print('Datenaufbereitung nach %f s erfolgreich.' % (tic2 - tic1))
	except Exception as e:
		print('Nach Datenaufbereitung konnte nicht in die Datei geschrieben werden, da:')
		print(str(e))

#print(np.load('dataset.npy').shape, np.load('output.npy').shape)

# Initialisieren der Simulation
animatebool = False
if animatebool == True:
	animations = defaultdict()
	for i in range(0,n):
		animations[i] = Animation(ax)
	def mergeAnimsInit():
		temp = [animations[i].AniInit() for i in range(0,n)]
		div = len(temp[0][:])
		return [temp[int(i*div**-1)][i%div] for i in range(n*div)]
	def mergeAnims(i):
		temp = [animations[j].animate(i,solutions['ddr'][j]) for j in range(0,n)]
		div = len(temp[0][:])
		return [temp[int(i*div**-1)][i%div] for i in range(n*div)]
	anim = animation.FuncAnimation(fig, mergeAnims, init_func=mergeAnimsInit, frames=NoSteps, interval=1, blit=True)

	# Sepichert animatio in mp4-Video
	#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
	plt.show()


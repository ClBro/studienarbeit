

#from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf
import numpy as np
from random import shuffle, random, uniform
from collections import defaultdict
import os
def sigmoid(z):
	return (1+np.exp(-z))**-1	
testcase = True
if testcase == False:
	with np.load('dataset.npz') as x:
		NoExamples, nu = x['arr_0'].shape[0], x['arr_0'].shape[1]	
		data = np.zeros((NoExamples, nu))
		data[:,0:nu] = x['arr_0']
	with np.load('output.npz') as y:
		ny = y['arr_0'].shape[1]
		data = np.append(data, y['arr_0'], 1)
else:
	print('Testdurchlauf wird ausgeführt')
	nu = 1
	ny = 1
	NoExamples = 500000
	data = np.zeros((NoExamples,nu+ny))	
	for i in range(NoExamples):
		data[i,0] = uniform(-np.pi/2, np.pi/2)
		for j in range(1,nu):
			data[i,j] = data[i,j-1]**(j+1)
		data[i,nu] = sigmoid(np.sin(data[i,0]))

eing = np.zeros((3,nu))
eing[:,0] = [0, np.pi/4, np.pi/2]
for i in range(1,nu):
	eing[:,i] = np.power(eing[:,i-1],i+1)

#data = np.zeros((2500000,7+6))
#data[:,0:7] = np.load('dataset.npy')
#data[:,7:13] = np.load('output.npy')
shuffle(data)
#data = data / data.max(axis=0)
ntrain = 0.6
ncv = 0.1
ntest = 1 - ntrain - ncv

print('Form des Datenarrays:',data.shape)
print('Anzahl Features:', nu)
print('Anzahl der Ausgangsgrößen:',ny)
print('Anzahl der Trainingsbeispiele', NoExamples)
print('Verhätnis von Trainingsset zu CV-set und Testset: %.0f / %.0f / %.0f'%(ntrain*100, ncv*100, ntest*100))
cv_errors = []
training_errors =  []
# Import data
# Create the model
for nounits in range(1,100):
	deflayer = [nu,nounits,ny]
	nlayers = len(deflayer)
	W = defaultdict()
	b = defaultdict()
	y = defaultdict()
	#x1 = tf.placeholder(tf.float32, [None, nu], name='x')
	y['y0'] = tf.placeholder(tf.float32, [None, nu], name='x')
	for i in range(nlayers-1):
		with tf.name_scope('Layer'+str(i)):
			W['W'+str(i)] = tf.Variable(tf.random_normal([deflayer[i], deflayer[i+1]]), name='W'+str(i)) 
			b['b'+str(i)] = tf.Variable(tf.random_normal([deflayer[i+1]]), name='b'+str(i))
			if i ==(nlayers-2): #letze Layer
				y['y'+str(i+1)] = tf.nn.sigmoid(tf.matmul(y['y'+str(i)], W['W'+str(i)]) + b['b' + str(i)])
			else: 
				y['y'+str(i+1)] = tf.nn.sigmoid(tf.matmul(y['y'+str(i)], W['W'+str(i)]) + b['b' + str(i)])


	# Define loss and optimizer
	y2 = y['y'+str(nlayers-1)]
	y_ = tf.placeholder(tf.float32, [None, ny], name='y_')
	with tf.name_scope('Loss'):
		cross_entropy = tf.losses.mean_squared_error(y_, y2)#tf.losses.mean_squared_error(y_, y2)
	with tf.name_scope('Training'):
		train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cross_entropy)


	sess = tf.InteractiveSession()
	#tf.global_variables_initializer().run()
	sess.run(tf.global_variables_initializer())

	# op's
	cost_train = tf.summary.scalar('TrainingLoss', cross_entropy)
	accuracy = tf.losses.mean_squared_error(y_, y2) 
	cv_val = tf.summary.scalar('Cross-Validation-error', accuracy)
	#cv_summary = tf.summary.merge_all()
	# Zuweisen der op's zu dem summary
	merged_summary = tf.summary.merge([cost_train])
	cv_summary = tf.summary.merge([cv_val])
	# Pfad zu den Dateien
	dir = os.path.dirname(os.path.abspath(__file__))
	print('Aktuelles Verzeichnis:',dir)
	logs_path = dir + '/logs/nn/'
	print('Logdateien wurden geschrieben nach:')
	print(logs_path)
	writer = tf.summary.FileWriter(logs_path, sess.graph)

	# Train
	iterations = 1000
	for i in range(iterations):
		offset = int(NoExamples*ntrain*iterations**-1)
		batch_xs, batch_ys = data[i*offset:(i+1)*offset,0:nu], data[i*offset:(i+1)*offset,nu:nu+ny]
		#if i%10 == 0:
		#	s_train = sess.run(merged_summary, feed_dict={y['y0']: batch_xs, y_: batch_ys})
		
		#	writer.add_summary(s_train,i)
		sess.run(train_step, feed_dict={y['y0']: batch_xs, y_: batch_ys})
	# Test trained model



	indcv = int(NoExamples*ntrain)
	indtest = int(NoExamples*(ntrain + ncv))
	#print(indcv,indtest)
	#s_cv = sess.run(cv_summary, feed_dict={y['y0']: data[indcv:indtest,0:nu], y_: data[indcv:indtest,nu:nu+ny]})
	#writer.add_summary(s_cv, nounits)
	
	cv_error = accuracy.eval(feed_dict={y['y0']: data[indcv:indtest,0:nu], y_: data[indcv:indtest,nu:nu+ny]})
	cv_errors.append(cv_error)
	#print('Cross-validation error:',cv_error)
	training_errors.append(cross_entropy.eval(feed_dict={y['y0']: data[indtest:NoExamples,0:nu], y_: data[indtest:NoExamples,nu:nu+ny]}))
	#print('Test error:',sess.run(accuracy, feed_dict={y['y0']: data[indtest:NoExamples,0:nu], y_: data[indtest:NoExamples,nu:nu+ny]}))
	 
	#eing = [[0,0],[np.pi/4,(np.pi/4)**2],[np.pi/2,(np.pi/2)**2]]
	#print('Input:', eing)
	#print('Predicting:',y2.eval(feed_dict={y['y0']: eing}))

import matplotlib.pyplot as plt
fig = plt.figure()
ax1 = plt.subplot('211')
ax1.plot(cv_errors, marker='o')
ax2 = plt.subplot('212')
ax2.plot(training_errors,marker='x')
plt.show()

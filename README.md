# README #

### What is this repository for? ###

* Daten zur Studienarbeit von Clemens Bromann

### How do I get set up? ###

* Der Ordner "Systemsimulation" enth�lt das Kinematische Modell in der Datei "ddr.py" und die Berechnung des neuronalen Netzes in nn.py
* ddr.py speichert die Trainingsdaten in dataset.npz und output.npz als numpy arrays
* nn.py liest die .npz files und trainiert ein NN
* In dem Ordner "logs/nn" sind die Daten zum Upload auf TensorBoard enthalten
* Der Ordner "Ausarbeitung" enth�lt die Datein des schriftlichen Teiles zug�hrig zum file "studienarbeit.tex"
